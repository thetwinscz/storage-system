﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageSystem;
using System.Data;
using StorageSystem.Entity.Storage;
using StorageSystem.Entity;
using StorageSystem.Utils;
using System.Windows.Controls;

namespace StorageSystem.Manager
{
    public class StorageManager : IDataStorage
    {
        protected DataTable dataTable;
        protected Storage storage;
        protected DataGrid items;
        protected ComboBox searchBox;

        public StorageManager(DataGrid items, ComboBox searchBox)
        {
            this.dataTable = new DataTable();
            this.items = items;
            this.searchBox = searchBox;
            this.PrepareDataTable();
            this.PrepareSearchComboBox();
            this.items.ItemsSource = dataTable.AsDataView();
            this.storage = new Storage();
        }

        public void AddProduct(Product product, int quantity)
        {
            StorageItem storageItem = this.storage.FindStorageItemByCode(product.Code);
            if (storageItem is null)
            {
                this.storage.CreateStorageItem(product, quantity);
            }
            else
            {
                storageItem.Quantity += quantity;
            }
            this.Reload();
        }

        public void DeleteProduct(Product product)
        {
            this.storage.RemoveStorageItem(product);
            this.Reload();
        }

        public Storage GetStorage()
        {
            return this.storage;
        }

        public void Reload()
        {
            this.dataTable.Clear();
            foreach (StorageItem item in this.storage.GetItems())
            {
                this.dataTable.Rows.Add(
                    item.Product.Code,
                    item.Product.Name,
                    item.Product.Category,
                    item.Product.ExpirationAt.Value.ToString("dd.MM.yyyy"),
                    item.Product.PriceBuy,
                    item.Product.PriceSell,
                    item.Quantity
                );
            }
        }

        public bool AddCategory(string name, Category parent, string path)
        {
            if (!this.storage.CheckIfCategoryExist(path))
            {
                this.storage.GetCategories().Add(new Category(name, parent, path));
                return true;
            }
            else
                MessageHelper.Error("Kategorie již existuje");
            return false;
        }

        public void ReloadItemsBySearching()
        {
            this.dataTable.Clear();
            foreach (StorageItem item in this.storage.GetSearchItems())
            {
                this.dataTable.Rows.Add(
                    item.Product.Code,
                    item.Product.Name,
                    item.Product.Category,
                    item.Product.ExpirationAt.Value.ToString("dd.MM.yyyy"),
                    item.Product.PriceBuy,
                    item.Product.PriceSell,
                    item.Quantity
                );
            }
        }

        private void PrepareDataTable()
        {
            this.dataTable.Columns.Add("Kód položky");
            this.dataTable.Columns.Add("Název položky");
            this.dataTable.Columns.Add("Kategorie");
            this.dataTable.Columns.Add("Trvanlivost");
            this.dataTable.Columns.Add("Nákupní cena");
            this.dataTable.Columns.Add("Prodejní cena");
            this.dataTable.Columns.Add("Počet kusů");
        }

        private void PrepareSearchComboBox()
        {
            searchBox.Items.Add("Číslo faktury");
            searchBox.Items.Add("Kód položky");
            searchBox.Items.Add("Název položky");
            searchBox.Items.Add("Kategorie");
            searchBox.Items.Add("Trvanlivost");
            searchBox.Items.Add("Nákupní cena");
            searchBox.Items.Add("Prodejní cena");
            searchBox.Items.Add("Počet kusů");
            searchBox.Items.Add("Datum přidání");
            searchBox.Items.Add("Dodavatel");
            searchBox.SelectedIndex = 0;
        }

        public void CreateStorageItemList(StorageItem storageItem)
        {
            foreach (StorageItem item in storage.GetSearchItems())
            {
                if (item != storageItem)
                {
                    this.storage.CreateStorageItem(storageItem.Product, storageItem.Quantity);
                }
            }
        }
    }
}
