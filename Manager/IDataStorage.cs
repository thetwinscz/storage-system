﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageSystem.Entity;
using StorageSystem.Entity.Storage;

namespace StorageSystem.Manager
{
    public interface IDataStorage
    {
        public void AddProduct(Product product, int quantity);
        public void DeleteProduct(Product product);
        //void EditProduct(string code);
        public void Reload();
        public bool AddCategory(string name, Category parent, string path);
        public void ReloadItemsBySearching();
        public Storage GetStorage();
    }
}
