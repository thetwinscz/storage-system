﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StorageSystem.Entity.Storage;
using StorageSystem.Entity;
using System.Data;
using StorageSystem.Utils;
using StorageSystem.Manager;

namespace StorageSystem
{
    public partial class MainWindow : Window
    {
        protected IDataStorage storageManager;

        public MainWindow()
        {
            InitializeComponent();
            this.itemsDataGrid.ColumnWidth = new DataGridLength(10, DataGridLengthUnitType.Star);
            this.storageManager = new StorageManager(itemsDataGrid, searchComboBox);
        }

        private void AddProductButton_Click(object sender, RoutedEventArgs e)
        {
            ProductForm productForm = new ProductForm(this.storageManager);
            productForm.ShowDialog();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.itemsDataGrid.SelectedIndex >= 0)
            {
                DataRowView selectedProduct = (DataRowView)itemsDataGrid.SelectedItem;
                if (
                    MessageHelper.YesOrNo(
                        string.Format(
                           "Opravdu chcete vymazat všechny vybrané položky? ({0}Ks)", selectedProduct.Row.ItemArray[6]
                        ), "Upozornění"
                    )
                )
                {
                    string selectedProductCode = selectedProduct.Row.ItemArray[0].ToString();
                    this.storageManager.DeleteProduct(new Product(selectedProductCode));
                }
            }
        }

        private void EditProduct(object sender, RoutedEventArgs e)
        {
            ProductForm productForm = new ProductForm(this.storageManager, true);
            if (this.itemsDataGrid.SelectedIndex >= 0)
            {
                DataRowView selectedProduct = (DataRowView)itemsDataGrid.SelectedItem;
                productForm.ShowEditProductForm(storageManager.GetStorage().FindStorageItemByCode(selectedProduct.Row.ItemArray[0].ToString()));
            }
        }

        private void ItemsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditProduct(sender, e);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            storageManager.GetStorage().GetSearchItems().Clear();
            string searchText = searchTextBox.Text;
            int index = searchComboBox.SelectedIndex;
            Storage.SearchCriteria attribut = (Storage.SearchCriteria)index;

            storageManager.GetStorage().SearchItemsByAttributeValue(attribut, searchText);
            storageManager.ReloadItemsBySearching();
        }
    }
}
