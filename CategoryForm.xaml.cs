﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StorageSystem.Entity;
using StorageSystem.Validator;
using StorageSystem.Manager;
using BasicFormValidator;
using BasicFormValidator.Model;
using StorageSystem.Utils;

namespace StorageSystem
{
    /// <summary>
    /// Interaction logic for CategoryForm.xaml
    /// </summary>
    public partial class CategoryForm : Window
    {
        protected ValidatorManager validator;
        protected IDataStorage storageManager;
        public CategoryForm(IDataStorage storageManager)
        {
            InitializeComponent();
            this.validator = new ValidatorManager(new ValidatorCustomMessageCollection());
            this.InitFormValidations();
            this.storageManager = storageManager;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Todo: opravit knihovnu validate
            /*if (this.validator.Validate())
            {*/
                TreeViewItem parent = categoryTreeView.Tag as TreeViewItem;
                string path = storageManager.GetStorage()
                    .CreatePathByTreeViewItem(parent);
                bool isAdded = this.storageManager.AddCategory(
                    categoryTextBox.Text,
                    storageManager.GetStorage().FindCategoryByCode(path),
                    string.Format(path + "---->>" + Convert.ToBase64String(Encoding.UTF8.GetBytes(categoryTextBox.Text)))
            );
                if (isAdded)
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = categoryTextBox.Text;
                    parent.Items.Add(item);
                }
            /*}
            else
            {
                MessageHelper.Error(this.validator.GetErrorsAsString());
            }*/
        }

        private void TreeViewItem_OnItemSelected(object sender, RoutedEventArgs e)
        {
            this.categoryTreeView.Tag = e.OriginalSource;
        }

        private void InitFormValidations()
        {
            this.validator.AddComponent(new ValidationComponent(this.categoryTextBox, "Zadejte název nové kategorie", true, 1));
        }
    }
}
