﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StorageSystem.Entity;
using StorageSystem.Utils;
using System.Data;
using StorageSystem.Entity.Storage;
using StorageSystem.Validator;
using StorageSystem.Manager;
using BasicFormValidator;
using BasicFormValidator.Model;

namespace StorageSystem
{
    public partial class ProductForm : Window
    {
        protected IDataStorage storageManager;
        protected ValidatorManager validator;

        public ProductForm(IDataStorage storageManager, bool isEdit = false)
        {
            InitializeComponent();
            this.storageManager = storageManager;
            this.validator = new ValidatorManager(new ValidatorCustomMessageCollection());
            this.InitFormValidations();
            if (isEdit)
            {
                AddButton.Visibility = Visibility.Collapsed;
                EditButton.Visibility = Visibility.Visible;
                barcodeTextBox.IsEnabled = false;
                invoiceNumberTextBox.IsEnabled = false;
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.validator.Validate())
            {
                this.storageManager.AddProduct(
                    new Product(
                        this.barcodeTextBox.Text,
                        int.Parse(this.BuyPriceTextBox.Text),
                        int.Parse(this.SellPriceTextBox.Text),
                        this.expirationDataPicker.SelectedDate,
                        DateTime.Now,
                        this.nameTextBox.Text,
                        new Category(this.categoryComboBox.Text),
                        this.supplierTextBox.Text,
                        this.invoiceNumberTextBox.Text
                    ),
                    int.Parse(this.quantityTextBox.Text)
                );
            }
            else
            {
                MessageHelper.Error(this.validator.GetErrorsAsString());
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.validator.Validate())
            {
               StorageItem StorageItem = storageManager.GetStorage()
                    .FindStorageItemByCode(this.barcodeTextBox.Text);
                StorageItem.Product.Name = nameTextBox.Text;
                StorageItem.Product.PriceBuy = int.Parse(BuyPriceTextBox.Text);
                StorageItem.Product.PriceSell = int.Parse(SellPriceTextBox.Text);
                StorageItem.Product.Supplier = supplierTextBox.Text;
                StorageItem.Quantity = int.Parse(quantityTextBox.Text);
                StorageItem.Product.ExpirationAt = expirationDataPicker.SelectedDate;
                StorageItem.Product.InvoiceNumber = invoiceNumberTextBox.Text;
                StorageItem.Product.LoadDataset();
                storageManager.Reload();
                Close();
            }
            else
            {
                MessageHelper.Error(this.validator.GetErrorsAsString());
            }
        }
            private void InitFormValidations()
        {
            this.validator.AddComponent(new ValidationComponent(this.invoiceNumberTextBox, "Číslo faktury", true, 4));
            this.validator.AddComponent(new ValidationComponent(this.barcodeTextBox, "Čárový kód", true));
            this.validator.AddComponent(new ValidationComponent(this.nameTextBox, "Název produktu", true));
            this.validator.AddComponent(new ValidationComponent(this.BuyPriceTextBox, "Nákupní cena", true));
            this.validator.AddComponent(new ValidationComponent(this.SellPriceTextBox, "Prodejní cena", true));
            this.validator.AddComponent(new ValidationComponent(this.supplierTextBox, "Dodavatel", true));
            this.validator.AddComponent(new ValidationComponent(this.quantityTextBox, "Počet kusů", true));
            this.validator.AddComponent(new ValidationComponent(
                this.expirationDataPicker,
                "Expirační datum",
                true,
                new DateTime(DateTime.Today.Date.Ticks))
            );
        }

        public void ShowEditProductForm(StorageItem storageItem)
        {
            barcodeTextBox.Text = storageItem.Product.Code;
            nameTextBox.Text = storageItem.Product.Name;
            BuyPriceTextBox.Text = storageItem.Product.PriceBuy.ToString();
            SellPriceTextBox.Text = storageItem.Product.PriceSell.ToString();
            supplierTextBox.Text = storageItem.Product.Supplier;
            quantityTextBox.Text = storageItem.Quantity.ToString();
            expirationDataPicker.Text = storageItem.Product.ExpirationAt.Value.ToString("dd.MM.yyyy");
            invoiceNumberTextBox.Text = storageItem.Product.InvoiceNumber;
            ShowDialog();
        }
    }
}
