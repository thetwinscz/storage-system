﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasicFormValidator; 
using BasicFormValidator.Collection;

namespace StorageSystem.Validator
{
    class ValidatorCustomMessageCollection : IValidatorMessageCollection
    {
      public Dictionary<ValidatorManager.Messages, string> getMessages()
        {
            return new Dictionary<ValidatorManager.Messages, string>
            {
                { ValidatorManager.Messages.Required, "{component} - Pole je povinné" },
                { ValidatorManager.Messages.MinDate, "{component} - Datum musí být od {dd.MM.yyyy}" },
                { ValidatorManager.Messages.MaxDate, "{component} - Datum musí být do {dd.MM.yyyy}" },
                { ValidatorManager.Messages.MinLength, "{component} - Minimální délka je {length}" }
            };
        }
    }
}
