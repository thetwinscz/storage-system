﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StorageSystem.Utils
{
    static class MessageHelper
    {
        public static void Error(string message)
        {
            MessageBox.Show(message, "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void Warning(string message)
        {
            MessageBox.Show(message, "Varování", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public static void Information(string message)
        {
            MessageBox.Show(message, "Informace", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static bool YesOrNo(string message, string header)
        {
            MessageBoxResult result = MessageBox.Show(message, header, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                return true;
            }
            return false;
        }
    }
}