﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using StorageSystem.Entity;

namespace StorageSystem.Utils
{
    static class StringHelper
    {
        public static Dictionary<char, int> diacriticsDictionary = new Dictionary<char, int>
        {
            { 'ě', 0 },
            { 'š', 1 },
            { 'č', 2 },
            { 'ř', 3 },
            { 'ž', 4 },
            { 'ý', 5 },
            { 'á', 6 },
            { 'í', 7 },
            { 'é', 8 },
            { 'ť', 9 },
            { 'ů', 10 },
            { 'ú', 11 },
            { 'ď', 12 }
        };

        public static string ConvertStringWithoutDiacritics(string text)
        {
            string newText = "";
            foreach (char letter in text)
            {
                if (diacriticsDictionary.ContainsKey(letter))
                    newText += diacriticsDictionary[letter];
                else
                    newText += letter;
            }
            return newText;
        }

        public static string GetItemPath(Category category)
        {
            Category item = category;
            string itemString = item.Name;
            while (item.Parent is not null)
            {
                itemString += item.Parent.Name;
                item = item.Parent is not null ? item.Parent : null;
            }
            return StringHelper.ConvertStringWithoutDiacritics(itemString.Replace(" ", "").ToLower());
        }
    }
}
