﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Globalization;
using StorageSystem.Entity.Storage;

namespace StorageSystem.Entity
{
    public class Product
    {
        public string Code { get; set; }
        public int PriceBuy { get; set; }
        public int PriceSell { get; set; }
        public DateTime? ExpirationAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Name { get; set; }
        public string Supplier { get; set; }
        public Category Category { get; set; }
        public string InvoiceNumber { get; set; }

        public Dictionary<Storage.Storage.SearchCriteria, string> dataset;

        public Product(string code)
        {
            this.Code = code;
        }

        public Product(
            string code,
            int priceBuy,
            int priceSell,
            DateTime? expirationAt,
            DateTime? createdAt,
            string name,
            Category category,
            string supplier,
            string invoiceNumber
        )
        {
            this.Code = code;
            this.PriceBuy = priceBuy;
            this.PriceSell = priceSell;
            this.ExpirationAt = expirationAt;
            this.CreatedAt = createdAt;
            this.Name = name;
            this.Category = category;
            this.Supplier = supplier;
            this.InvoiceNumber = invoiceNumber;
            this.LoadDataset();
        }

        public Dictionary<Storage.Storage.SearchCriteria, string> GetDataset()
        {
            return this.dataset;
        }

        public void LoadDataset()
        {
            this.dataset = new Dictionary<Storage.Storage.SearchCriteria, string>
            {
                { Storage.Storage.SearchCriteria.Code, this.Code },
                { Storage.Storage.SearchCriteria.Name, this.Name },
                { Storage.Storage.SearchCriteria.Category, this.Category.ToString() },
                { Storage.Storage.SearchCriteria.ExpirationAt, this.ExpirationAt.Value.ToString("dd.MM.yyyy") },
                { Storage.Storage.SearchCriteria.PriceBuy, this.PriceBuy.ToString() },
                { Storage.Storage.SearchCriteria.PriceSell, this.PriceSell.ToString() },
                { Storage.Storage.SearchCriteria.InvoiceNumber, this.InvoiceNumber },
                { Storage.Storage.SearchCriteria.CreatedAt, this.CreatedAt.Value.ToString("dd.MM.yyyy") },
            }; 
        }
    }
}
