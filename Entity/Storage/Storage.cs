﻿using System.Collections.Generic;
using System.Data;
using System;
using StorageSystem.Utils;
using System.Windows.Controls;
using System.Windows;
using System.Text;

namespace StorageSystem.Entity.Storage
{
    public class Storage
    {
        protected DataTable s = new DataTable();
        protected List<StorageItem> storageItems;
        protected List<StorageItem> searchItems;
        protected List<Category> categories;
        protected StorageItem storageItem;
        public enum SearchCriteria {
            InvoiceNumber = 0,
            Code = 1,
            Name = 2,
            Category = 3,
            ExpirationAt = 4,
            PriceBuy = 5, 
            PriceSell = 6, 
            Quantity = 7,
            CreatedAt = 8,
            Supplier = 9, 
        }

        public Storage()
        {
            this.storageItems = new List<StorageItem>();
            this.categories = new List<Category>();
            this.searchItems = new List<StorageItem>();
        }

        public void SearchItemsByAttributeValue(SearchCriteria searchCriteria, string value)
        {
            foreach (StorageItem storageItem in this.storageItems)
            {
                if (searchCriteria == SearchCriteria.Quantity)
                {
                    if (storageItem.Quantity == int.Parse(value))
                    {
                        this.searchItems.Add(storageItem);
                    }
                }
                else
                {
                    Product product = storageItem.Product;
                    var dataset = product.GetDataset();
                    if (dataset[searchCriteria].Contains(value))
                    {
                        this.searchItems.Add(storageItem);
                    }
                }
            }
        }

        public Product FindProductByCode(string code)
        {
            foreach (StorageItem storageItem in storageItems)
            {
                if (storageItem.Product.Code == code)
                {
                    return storageItem.Product;
                }
            }

            return null;
        }

        public bool IsProductExist(Product product)
        {
            foreach (StorageItem storageItem in this.storageItems)
            {
                if (storageItem.Product.Code == product.Code)
                {
                    return true;   
                }
            }

            return false;
        }

        public StorageItem CreateStorageItem(Product product, int quantity = 1)
        {
            StorageItem item = new StorageItem(product, quantity);
            this.storageItems.Add(item);
            return item;
        }

        public void RemoveStorageItem(Product product)
        {
            this.storageItems.Remove(FindStorageItemByCode(product.Code));
        }        

        public List<StorageItem> GetItems()
        {
            return this.storageItems;
        }

        public List<StorageItem> GetSearchItems()
        {
            return this.searchItems;
        }

        public StorageItem FindStorageItemByName(string name)
        {
            foreach (StorageItem storageItem in this.storageItems)
            {
                if (storageItem.Product.Name.Contains(name))
                {
                    this.searchItems.Add(storageItem);
                    return storageItem;
                }
            }
            return null;
        }

        public StorageItem FindStorageItemByCode(string code)
        {
            foreach (StorageItem storageItem in storageItems)
            {
                if (storageItem.Product.Code == code)
                {
                    return storageItem;
                }
            }
            return null;
        }

        public void FindItemByName(string name)
        {
            foreach (StorageItem storageItem in this.storageItems)
            {
                if (storageItem.Product.Name.Contains(name))
                {
                    searchItems.Add(storageItem);
                }
            }
        }
        public bool CheckIfCategoryExist(string path)
        {
            foreach (Category category in categories)
            {
                if (category.Path == path)
                    return true;
            }
            return false;
        }

        public Category FindCategoryByCode(string path)
        {
            foreach (Category category in categories)
            {
                if (category.Path == path)
                    return category;
            }
            return null;
        }

        public string CreatePathByTreeViewItem(TreeViewItem firstItem)
        {
            TreeViewItem item = firstItem;
            string itemString = "";
            while (item.Parent is TreeViewItem)
            {
                string header = (item.Parent as TreeViewItem).Header.ToString();
                itemString += Convert.ToBase64String(Encoding.UTF8.GetBytes(header)) + "---->>";
                item = item.Parent is TreeViewItem ? item.Parent as TreeViewItem : null;
            }
            itemString += Convert.ToBase64String(Encoding.UTF8.GetBytes(firstItem.Header.ToString()));
            return itemString;
        }

        public List<Category> GetCategories()
        {
            return this.categories;
        }
    }
}
