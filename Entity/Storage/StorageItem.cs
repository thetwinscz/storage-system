﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StorageSystem.Entity.Storage
{
    public class StorageItem
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }

        public StorageItem(Product product, int quantity)
        {
            this.Product = product;
            this.Quantity = quantity;
        }
    }
}
