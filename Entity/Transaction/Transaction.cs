﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageSystem.Entity.Storage;

namespace StorageSystem.Entity.Transaction
{
    class Transaction
    {
        protected List<TransactionItem> transactionItems;
        public int TotalPrice { get; set; }
        public DateTime TransactionDate { get; set; }

        public List<TransactionItem> GetTransactionItems()
        {
            return this.transactionItems;
        }
        public Transaction AddStorageProduct(StorageItem storageItem, int quantity)
        {
            this.transactionItems.Add(new TransactionItem(storageItem, storageItem.Product.Code, quantity));
            return this;
        }
    }
}
