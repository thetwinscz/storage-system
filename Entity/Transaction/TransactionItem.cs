﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageSystem.Entity.Storage;

namespace StorageSystem.Entity.Transaction
{
    class TransactionItem
    {
        public StorageItem StorageItem { get; set; }
        public string Code { get; set; }
        public int Quantity { get; set; }
        public bool IsImport { get; set; }

        public TransactionItem(
            StorageItem storageItem,
            string code, int quantity,
            bool isImport = false
        )
        {
            this.StorageItem = storageItem;
            this.Code = code;
            this.Quantity = quantity;
        }
    }
}
