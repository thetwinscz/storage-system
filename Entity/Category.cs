﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StorageSystem.Entity
{
    public class Category
    {
        public Category Parent { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }

        public Category(string name, Category parent = null, string path = null)
        {
            this.Name = name;
            this.Parent = Parent;
            this.Path = path;
        }

        public override string ToString()
        {
            return string.Format(this.Name);
        }
    }
}
