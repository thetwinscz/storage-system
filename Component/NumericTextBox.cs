﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StorageSystem.Component
{
    public class NumericTextBox : TextBox
    {
        static NumericTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NumericTextBox), new FrameworkPropertyMetadata(typeof(NumericTextBox)));
        }

        public NumericTextBox()
        {
            this.PreviewTextInput += new TextCompositionEventHandler(this.CheckNumericValue);
        }

        protected void CheckNumericValue(object sender, TextCompositionEventArgs e)
        {
            int output;
            if (!int.TryParse(e.Text, out output))
            {
                e.Handled = true;
            }
        }

    }
}
